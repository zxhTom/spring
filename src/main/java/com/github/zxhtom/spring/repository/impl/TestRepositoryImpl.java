package com.github.zxhtom.spring.repository.impl;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.github.zxhtom.spring.repository.impl
 * @date 2020年06月03日, 0003 13:47
 * @Copyright © 2020 安元科技有限公司
 */
@Repository
public class TestRepositoryImpl implements TestRepository {

  @Override
  public Map<String, Object> test(Map<String, Object> paramMap) {
    System.out.println("测试流程通过ss");
    paramMap.size();
    return null;
  }
}
