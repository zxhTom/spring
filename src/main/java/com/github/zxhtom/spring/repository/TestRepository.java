package com.github.zxhtom.spring.repository;

import java.util.Map;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.github.zxhtom.spring.repository
 * @date 2020年06月03日, 0003 13:46
 * @Copyright © 2020 安元科技有限公司
 */
public interface TestRepository {
    /**
    * @author zxhtom
    * @Description test
    * @Date 13:47 2020年06月03日, 0003
    * @Param 
    * @return java.util.Map<java.lang.String,java.lang.Object>
    */
    Map<String, Object> test(Map<String, Object> paramMap);
}
