package com.github.zxhtom.spring.service;

import java.util.Map;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.github.zxhtom.spring.service
 * @date 2020年06月03日, 0003 13:45
 * @Copyright © 2020 安元科技有限公司
 */
public interface TestService {
  /**
   * @author zxhtom
   * @Description test
   * @Date 13:46 2020年06月03日, 0003
   * @Param
   * @return java.util.Map<java.lang.String,java.lang.Object>
   */
  Map<String, Object> test(Map<String, Object> paramMap);
  Map<String, Object> hello();
}
