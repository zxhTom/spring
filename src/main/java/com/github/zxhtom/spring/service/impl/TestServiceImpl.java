package com.github.zxhtom.spring.service.impl;

import com.github.zxhtom.spring.repository.TestRepository;
import com.github.zxhtom.spring.service.TestService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.github.zxhtom.spring.service.impl
 * @date 2020年06月03日, 0003 13:45
 * @Copyright © 2020 安元科技有限公司
 */
@Service
public class TestServiceImpl implements TestService {
  @Autowired private TestRepository testRepository;

  @Override
  public Map<String, Object> test(Map<String, Object> paramMap) {
    return testRepository.test(paramMap);
  }
}
