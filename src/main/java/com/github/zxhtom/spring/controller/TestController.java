package com.github.zxhtom.spring.controller;

import com.github.zxhtom.spring.service.TestService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.github.zxhtom.spring.controller
 * @date 2020年06月03日, 0003 13:43
 * @Copyright © 2020 安元科技有限公司]
 * @description controller控制器
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {
  @Autowired private TestService testService;

  @RequestMapping(value = "/start", method = RequestMethod.GET)
  public Map<String, Object> test(@RequestParam Map<String, Object> paramMap) {
    Map<String, Object> test = testService.test(paramMap);
    return test;
  }
}
